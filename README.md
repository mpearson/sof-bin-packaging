# sof-bin-packaging

First attempt at doing sof-bin package

Steps:

	git clone https://salsa.debian.org/mpearson/sof-bin-packaging.git
	git clone -b stable-v1.6 https://github.com/thesofproject/sof-bin.git
	tar -zcf sof-bin_1.6.orig.tar.gz sof-bin
	cd sof-bin
	cp -r ../sof-bin-packaging/debian .
	debuild -us -uc

